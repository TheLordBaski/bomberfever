var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var fs = require('fs');
var url = require('url');
var path = require('path');
var express = require('express');
var databaseUrl = "bomberfever";
var collections = ["User","RankedFFA"]
/* User:{username,password,email,socketId,online,rank,priority,findingRanked,friends[username],gamesPlayed,gamesWon,gamesLeft]}
 * RankedFFA:{finder,players[username,rank,ready(yes/no/dec)],waiting,playing}
 */
var db = require("mongojs").connect(databaseUrl, collections);

var continueFinding = true;
var serverTime = 0;
	

app.get('/', function(req, res){
	res.sendFile(__dirname + "/index.html");
});

app.get('/game.html',function(req, res){
	id = url.parse(req.url, true).query.id;
	
	if(id === undefined) res.sendFile(__dirname + "/logerr.html");
	else res.sendFile(__dirname + "/game.html");
});

app.get('/lobby.html',function(req, res){
	id = url.parse(req.url, true).query.id;
	
	if(id === undefined) res.sendFile(__dirname + "/logerr.html");
	else res.sendFile(__dirname + "/lobby.html");
});

app.use('/css', express.static(path.join(__dirname, '/css')));
app.use('/js', express.static(path.join(__dirname, '/js')));




io.on('connection', function(socket){
	/* Obsluha prihlaseni na server
	 *  kontrola registrace ctu v databazi
	 *  kontrola zda uzivatel jiz neni prihlasen
	 *  kontrola hesla */
	socket.on('login',function(msg){
		db.User.find({username:msg['username']},function(err,matches){
			if(matches.length === 0){
				console.log('User '+msg['username']+' is not registered');
				io.to(socket.id).emit('logon',"Username or password is not valid");
			}
			else {
				if(matches[0].password === msg['password']){
					if(matches[0].online){
						console.log('User '+msg['username']+' is already connected');
						io.to(socket.id).emit('logon',"User is already connected");
					}
					else {
						db.User.update({"username":msg['username']},{$set :{"socketId":socket.id,"online":true}});
						console.log('User '+msg['username']+' connected');
						io.to(socket.id).emit('logon',true);
					}
				}
				else {
					console.log('Wrong password');
					io.to(socket.id).emit('logon',"Username or password is not valid");
				}	
			}
		});
	});		

	/* Obsluha registrace 
	 * 	kontrola databaze na email/username kvuli pripadnym duplicitnim registracim
	 *  ulozeni uzivatele do databaze */
	socket.on('registerme',function(msg){
		db.User.find({$or: [{"username":msg['username']},{"mail":msg['mail']}]},function(err,matches){
			if(matches.length !== 0){
				console.log('User '+msg['username']+' is already registered');
				io.to(socket.id).emit('registered',"Given username is already used");
			}
			else {
				db.User.save({
					username: msg['username'],
					password: msg['password'],
					mail: msg['mail'],
					socketId: socket.id,
					online: true,
					rank: 1000,
					priority: 0,
					findingRanked: false,
					penalty:0,
					gamesPlayed:0,
					gamesWon:0,
					gamesLeft:0	
				}, function (err, saved) {
					if (err || !saved) {
						console.log('Error occured when registering');
						io.to(socket.id).emit('registered',"An error occured when registering user");
					}
					else {
						console.log('User '+msg['username']+' registered');
						io.to(socket.id).emit('registered',true);
					}
				});		
			}
		});
	});
	
	/* Obsluha hledani ranked zapasu FFA
	 * V pripade ze uzivatel nema zakazano hledat hru, je jeho stav nastaven jako "hledajici" */
	socket.on('findRanked',function(msg){
		db.User.find({"socketId":socket.id},function(err,player){
			if(player.length !== 0){
				if(player[0].penalty <= serverTime){
					db.User.update({"socketId":socket.id},{$set:{"findingRanked":true,"priority":40}});
					io.to(socket.id).emit("findingGame",0);
				}
				else {
					io.to(socket.id).emit("findingGame",player[0].penalty-serverTime);
				}
			}
		});
	});	
	
	/* Obsluha odpojeni od serveru */
	socket.on('disconnect',function(msg){
		disconnectCorrectly(socket.id);
	});
	
	/* Obsluha prijeti nalezeneho ranked FFA zapasu */
	socket.on('acceptedFFA',function(msg){
		db.User.find({"socketId":socket.id},function(err,player){		// nalezeni hrace kvuli jeho username
			if(player.length !== 0){
				db.RankedFFA.update({"players":{$elemMatch:{"username":player[0].username}}},{$set:{"players.$.ready":"yes"}});		// nastaveni priznaku prijeti zapasu
				db.RankedFFA.find({"players":{$elemMatch:{"username":player[0].username}}},function(err,matchId){					// nalezeni ID zapasu kvuli usernames protihracu
					if(matchId.length !== 0){
						db.RankedFFA.find({"_id":matchId[0]._id},function(err,match){												// nalezeni zapasu podle ID
							if(match.length !== 0){
								for(var i=0;i<match[0].players.length;i++){														// zaslani informace o prijeti ostatnim hracum
									if(match[0].players[i].username !== player[0].username){
										db.User.find({"username":match[0].players[i].username},function(err,receiver){
											if(receiver.length !== 0){
												io.to(receiver[0].socketId).emit("playersReadyFFA",true);
											}
										});
									}
								}	
							}
						});
					}
				});
			}
		});
	});
	
	/* Obsluha odmitnuti nalezeneho ranked FFA zapas */
	socket.on('declinedFFA',function(msg){
		db.User.find({"socketId":socket.id},function(err,player){																	// nalezeni hrace kvuli jeho username
			if(player.length !== 0){
				db.RankedFFA.update({"players":{$elemMatch:{"username":player[0].username}}},{$set:{"players.$.ready":"dec"}});		// nastaveni priznaku odpojeni se ze zapasu
				db.RankedFFA.find({"players":{$elemMatch:{"username":player[0].username}}},function(err,matchId){					// nalezeni ID zapasu kvuli usernames uzivatelu
					if(matchId.length !== 0){
						db.RankedFFA.find({"_id":matchId[0]._id},function(err,match){												// nalezeni zapasu pomoci ID
							if(match.length !== 0){
								for(var i=0;i<match[0].players.length;i++){														// informovani ostatnich o odmitnuti zapasu
									if(match[0].players[i].username !== player[0].username){
										db.User.find({"username":match[0].players[i].username},function(err,receiver){
											if(receiver.length !== 0){
												io.to(receiver[0].socketId).emit("playersReadyFFA",false);
											}
										});
									}
								}
							}
						});
					}
				});
			}
		});
	});
	
	/* Obsluha konce vyhledavani zapasu */
	socket.on('stopSearching',function(msg){
		db.User.update({"socketId":socket.id},{$set:{"findingRanked":false}});
	});
	
});


/* Funkce pro spravu vyhledavani ranked zapasu FFA
 * Zapina se v pravidelnem intervalu (5sec), dokud je dostatek vyhledavajicich hracu, vybira prave toho s nejvyssi prioritou,
 * kteremu vybere vhodne protihrace
 */ 
function handleRankedFFA(){
	setInterval(function(){
		var pmin = 2;									// pocet hracu v zapasu ( default 6)
		var continueFinding = true						// rizeni hlavniho cyklu funkce
		var unfound = [];								// nepodarilo se nalezt hraci hru, je docasne vyrazen z vyhledavani a ulozen do unfound
		while(continueFinding){
			continueFinding = false;
			var count = 1;
			db.User.find({"findingRanked":true}).sort({"priority":-1},function(err,players){  // seznam vsech vyhledavajicich hracu serazen podle priority
			if(players.length>=pmin){															// nedostatek hracu -> konec 
				var finder = players[0];														// hrac s nejvyssi prioritou, kteremu jsou vyhledavani vhodni souperi
				var found = false;																// flag s informaci o nalezeni hry
				db.RankedFFA.save({																// docasne vytvoreni hry
					"finder":finder.username,
					"players":[{"username":finder.username,"rank":finder.rank,"ready":"no"}],
					"waiting":false,
					"playing":false
				});
				
				// pruchod ostatnimi hraci - hledani vhodnych protihracu a jejich vlozeni do docasneho objektu hry
				
				mylabel: for(var i = 1;i<players.length;i++){
					if((finder.rank <= (players[i].rank+players[i].priority)) && (finder.rank >= (players[i].rank-players[i].priority)) && count < pmin){
						db.RankedFFA.update({"finder":finder.username},{$push:{"players":{"username":players[i].username,"rank":players[i].rank,"ready":"no"}}});
						count++;
					}
					// nalezen dostatecny pocet hracu
					if(count === pmin){
						found = true;
						db.RankedFFA.find({"finder":finder.username},function(err,match){
							if(match.length !== 0){
								if(match[0].players.length < pmin){
									i--;
									return true;
								}
								for(var j=0;j<match[0].players.length;j++){
									db.User.update({"username":match[0].players[j].username},{$set:{"findingRanked":false}});
									db.User.find({"username":match[0].players[j].username},function(err,gamePlayers){
										if(gamePlayers.length !== 0) {						// vsichni hraci ve hre jsou informovani o uspesnem nalezeni protihracu a pozadani o prijeti hry
											console.log()
											io.to(gamePlayers[0].socketId).emit('foundGame',true);
										}
									});
								}
								db.RankedFFA.update({"finder":finder.username},{$set:{"waiting":true}});
								waitForPlayersFFA(finder.username,pmin);					// rizeni prijimani hry
								console.log('User '+finder.username+' found a game');
								count=0;
							}
						});
						break;	
					}
				}
				if(found === false){									
					db.RankedFFA.remove({"finder":finder.username});	// nebyli nalezeni protihraci - docasna hra je smazana
					unfound.push(finder.username);						// vyhledavajici hrac je docasne vyrazen z vyhledavani
				}
				continueFinding = true;		
			}
			});
		}
		
		// navrat docasne vyrazenych hracu zpet do vyhledavani
		for(var i = 0;i<unfound.length;i++){
			db.User.update({"username":unfound[i]},{$set:{"findingRanked":true}});
		}
		// zvyseni priotity vyhledavajicich hracu
		db.User.update({"findingRanked":true},{$inc:{"priority":5}},{"multi":true});
	},5000);
}


/* Funkce ridici cekani na prijeti hry 			// DODELAT prepsat komplet zasilani aktualizaci acceptu
 */ 
function waitForPlayersFFA(username,pmin){								// username: jmeno vyhledavajiciho hrace zapasu, pmin: pocet hracu zapasu
	var timer = 30;														// na prijeti vsech hracu se ceka 30s
	var timerId = setInterval(function(){								// kazdou vterinu je provedena kontrola stavu prijeti
		var plReady = 0;												// pocet pripravenych hracu
		db.RankedFFA.find({"finder":username},function(err,match){
			if(match.length !== 0){
				if(timer === 0){										// osetreni pripadu vyprseni casu:
					console.log('A player didnt accept a match');
					for(var i=0;i<match[0].players.length;i++){
						if(match[0].players[i].ready === "yes"){		// hraci kteri prijali zapas budou vyhledavat znovu
							db.User.update({"username":match[0].players[i].username},{$set:{"findingRanked":true}});
							db.User.find({"username":match[0].players[i].username},function(err,player){
								if(player.lenght !== 0){
									io.to(player[0].socketId).emit("findAgainFFA",true);
								}
							});
						}
						if(match[0].players[i].ready === "no"){			// ti kteri neprijali jsou pouze informovani o ukonceni vyhledavani a potrestani za narusovani behu hry
							db.User.find({"username":match[0].players[i].username},function(err,player){
								if(player.lenght !== 0){
									db.User.update({"username":player[0].username},{$set:{"penalty":serverTime+120}});
									io.to(player[0].socketId).emit("findAgainFFA",false);
								}
							});
						}
					}
					db.RankedFFA.remove({"finder":username});			// docasna hra je odstranena
					clearInterval(timerId);
				}
				else {													// jeste zbyva cas - osetreni prijeti/odmitnuti hry
					for(var i=0;i<match[0].players.length;i++){
						if(match[0].players[i].ready === "dec"){		// nekdo odmitnul:
							console.log('A player declined a match');
							for(var j=0;j<match[0].players.length;j++){	// ostatni jsou informovani a budou vyhledavat znovu
								if(match[0].players[j].username !== match[0].players[i].username){
									db.User.update({"username":match[0].players[j].username},{$set:{"findingRanked":true}});
									db.User.find({"username":match[0].players[j].username},function(err,player){
										if(player.lenght !== 0){
											io.to(player[0].socketId).emit("findAgainFFA",true);
										}
									});
								}
								else {									// ten kdo odmitl je informovan o konci vyhledavani
									db.User.find({"username":match[0].players[j].username},function(err,player){
										if(player.lenght !== 0){
											db.User.update({"username":player[0].username},{$set:{"penalty":serverTime+120}});
											io.to(player[0].socketId).emit("findAgainFFA",false);
										}
									});
								}
							}
							db.RankedFFA.remove({"finder":username});	// odstraneni docasne hry
							clearInterval(timerId);
						}
						if(match[0].players[i].ready === "yes"){		// hrac prijal
							plReady++;
						}
						if(plReady === pmin){							// vsichni hraci prijali:
							console.log('All players accepted a match');
							for(var j=0;j<match[0].players.length;j++){	// vsichni informovani o zacatku hry
								db.User.find({"username":match[0].players[j].username},function(err,player){
									if(player.lenght !== 0){
										io.to(player[0].socketId).emit("gameReadyFFA",true);
									}
								});
							}
							db.RankedFFA.update({"finder":username},{$set:{"waiting":false,"playing":true}});
							setTimeout(function(){
								db.RankedFFA.remove({"finder":username});
							},10000);
							clearInterval(timerId);
						}
					}
					timer--;
				}
			}	
		});
	},1000)
}

// Jednodchy counter ktery pocita sekundy od zacatku behu serveru
function handleServerTime(){
	var MAX_TIME=9007199254740000;
	setInterval(function(){
		if(serverTime === MAX_TIME){
			db.User.update({"penalty":{$gt:serverTime}},{$inc:{"penalty":-serverTime}},{"multi":true});
			serverTime = 0;
		}
		serverTime++;
		//console.log(serverTime);
	},1000);
}

/* Obsluha odpojeni od serveru
 * nastaveni priznaku odpojeni v aktualni hre a pripadne nastaveni pripadneho zakazu vyhledavani + zvyseni poctu opustenych her
 * vynulovani ID socketu a vynulovani priznaku "online" */
function disconnectCorrectly(id){
	db.User.find({"socketId":id},function(err,player){																	// nalezeni uzivatele kvuli jeho username
		if(player.length !== 0) {
			console.log('User '+player[0].username+' disconnected');
			db.RankedFFA.find({"players":{$elemMatch:{"username":player[0].username}}}, function(err,matchId){					// nalezeni ID zapasu kvuli usernames protihracu
				if(matchId.length !== 0){
					db.RankedFFA.find({"_id":matchId[0]._id},function(err,match){												// nalezeni zapasu podle ID
						if(match.length !== 0){
							db.User.update({"username":player[0].username},{$inc:{"gamesLeft":1}});								// pocet opustenych her ++
							if(match[0].waiting){
								db.RankedFFA.update({"players":{$elemMatch:{"username":player[0].username}},"playing":true},{$set:{"players.$.ready":"dec"}});	// nastaveni priznaku odmitnuti zapasu
								for(var i=0;i<match[0].players.length;i++){													// informovani ostatnich hracu o odmitnuti nekoho z protihracu
									if(match[0].players[i].username !== player[0].username){
										db.User.find({"username":match[0].players[i].username},function(err,receiver){
											if(receiver.length !== 0){
												io.to(receiver[0].socketId).emit("playersReadyFFA",false);
											}
										});
									}
								}
							}
							if(match[0].playing){
								db.RankedFFA.update({"players":{$elemMatch:{"username":player[0].username}},"playing":true},{$set:{"players.$.ready":"no"}});	// nastaveni priznaku opusteni zapasu
							}
						}
					});	
				}
			});
		}
	});
	db.User.update({"socketId":id},{$set :{"socketId":'0',"online":false,"findingRanked":false}});		// nastaveni "offline" stavu hrace po odpojeni	
}

/* Inicializace serveru
 * Inicializace databaze
 * Spusteni ridicich funkci */
http.listen(3000, function(){
	console.log('listening on *:3000');
	db.RankedFFA.remove({});
	db.User.update({},{$set:{"socketId":'0',"online":false,"findingRanked":false,"penalty":0}},{"multi":true});
	handleServerTime();
	handleRankedFFA();
});

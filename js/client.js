/*
 * =======================
 *       BomberFever
 * =======================
 * 
 * Depends on:		jquery
 * 
 * --------
 * Summary:
 * --------
 * Authors: Jakub Gabčo & Zdeněk Pečínka
 * Description: Client script that handles communication with server, bind evens of elements of DOM, etc...
 */
/*
 * Variables
 */
var socket = io();
var username = null;
var acceptPlayersNum = 0;
var context = 'login';
var keys = {
	'ENTER' : 13
}
/*Client-server communication*/

socket.on('registered',function(msg){
	if(msg == true){
		$('#registration').hide();
		$('.loading').show();
		replaceBodyMenu(4000);
		$('.notif').notifyMe(
			'bottom',
			'info',
			'You have been registred.',
			'Wait until game will load.',
			200,
			4000
		);		
	}
	else{
		produceLoginError('registration',msg);
		console.log(msg);
	}
});
/*
 * Socket recieve findingGame command
 * @timer Number of seconds that player can't find match. 
 */
socket.on('findingGame',function(timer){
	if(timer == 0){
		// Start timer
		$("#t").show().timer({
	        action: 'start',
	        seconds: 0
	      });
		// Hide all finding buttons/select boxes
		$('#server-select').hide();
		$('#mode-select').hide();
		$('#search').hide();
		$('#searching-game').show();
	}else{
		$('.notif').notifyMe(
			'bottom',
			'error',
			'You declined the ranked match',
			'You have '+timer+' seconds ban to finding a ranked match',
			200,
			4000
		);
	}
});
/*
 * Socket recieve logon commnd, Check if user can login into game
 * @msg Boolean variable true -> can login -> redraw DOM 
 */
socket.on('logon',function(msg){
	if(msg == true){
		context = 'logon';
		replaceBodyMenu(4000);
		$('.notif').notifyMe(
			'bottom',
			'info',
			'You have been logged in',
			'Wait until game will load.',
			200,
			4000
		);
	}else{
		$('#login').show();
		$('.loading').hide();
		produceLoginError('login',msg);
	}
});
/*
 * Socket recieve foundGame command, show accept screen
 * @msg always true(if not hacked)
 */
socket.on('foundGame',function(msg){
	if(msg){
		$("#t").hide().timer("remove");
		$('#found-game').show();
		$('#accept-game').show();
		$('#decline-game').show();
	}
});
/*
 * Socket recieve playerReadyFFA , Show that player accepted/declined match
 * data Boolean value if player accepted or declined match
 */
socket.on('playersReadyFFA',function(data){
	acceptPlayersNum += 1;
	if(data){
		$('.box'+acceptPlayersNum).addClass('accepted');
	}else{
		$('.box'+acceptPlayersNum).addClass('declined');
	}
});

socket.on('gameReadyFFA',function(data){
	if(data){
		clearAcceptMenu();
		console.log('Game found');
		//Redraw menu to game
		$('div').not('script').remove();
		$('body').prepend('<h1>GAAAMEEE</h1>');
	}
});

socket.on('findAgainFFA',function(msg){
	if(msg){
		// Start finding game again with higher priority
		$('#found-game').hide();
		$('#server-select').hide();
		$('#mode-select').hide();
		$('#search').hide();
		$('#searching-game').show();
		$("#t").show().timer({
	        action: 'start',
	        seconds: 0
	      });
		clearAcceptMenu();
		$('.notif').notifyMe(
			'bottom',
			'info',
			'Player declined ranked match.',
			'You are returned to matchmaking queue with higher priority.',
			200,
			4000
		);
	}else{
		clearAcceptMenu();
		$('#server-select').show();
		$('#mode-select').show();
		$('#search').show();
		$('#searching-game').hide();
		// Hide accept menu
		$('#found-game').hide();
		// Ban for 120 seconds to find a match
		$('.notif').notifyMe(
				'bottom',
				'error',
				'You declined the ranked match',
				'You have 120 seconds ban to finding a ranked match',
				200,
				4000
			);
	}
});

/* 
 * Bind events to elements of DOM
 */

$(document).on('keydown',function(e){
	if(e.keyCode == keys.ENTER){
		switch(context){
			case 'login':
				login();
				break;
			case 'register':
				register();
				break;
			case 'menu':
				break
			default:
				break;
		}
		
	}
});
$('#logmein').on('click',function(){
	login();
});


$('#register').on('click',function(){
	$('#login').hide();
	$('#registration').show();
	context = 'register';
});

$('#back').on('click',function(){
	$('#registration').hide();
	$('#login').show();
});

$('#registerme').on('click',function(){
	register();
});

/*
 * Function section
 */

/*
 * Function: produceLoginError
 * @param id
 * @param data
 */
function produceLoginError(id,data){
		$('.notif').notifyMe(
			'bottom',
			'error',
			'Login',
			data,
			200,
			2000		
		);
}

/*
 * Function: validateEmail
 * @param email
 */

function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

/*
 * Function: removeClass
 * @param cl
 */

function removeClass(cl){
	$(cl).removeClass(cl);
}

/*
 * Function: bindMenuEvents
 * Bind events on menu button after redrawing menu
 */

function bindMenuEvents(){
	$('#settings').on('click',function(){
		switchContext('options');
		$('.active').removeClass('active');
		$(this).addClass('active');
	});
	$('#me').on('click',function(){
		switchContext('shop');
		$('.active').removeClass('active');
		$(this).addClass('active');
	});
	$('#watch-game').on('click',function(){
		switchContext('watch');
		$('.active').removeClass('active');
		$(this).addClass('active');
	});

	$('#ranked-game').on('click',function(){
		switchContext('ranked');
		$('.active').removeClass('active');
		$(this).addClass('active');
	});

	$('#lobby-game').on('click',function(){
		switchContext('lobby');
		$('.active').removeClass('active');
		$(this).addClass('active');
	});
	$('#search').on('click',function(){
		socket.emit('findRanked');
	});
	$('#searching-game').on('click',function(){
		$('#server-select').show();
		$('#mode-select').show();
		$('#search').show();
		$(this).hide();
		$("#t").hide().timer("remove");
		socket.emit('stopSearching');
	});
	$('#accept-game').on('click',function() {
		$(this).hide();
		$('#decline-game').hide();
		acceptPlayersNum += 1;
		$('.box'+acceptPlayersNum).addClass('accepted');
		socket.emit('acceptedFFA');
	});
	$('#decline-game').on('click',function() {
		$(this).hide();
		$('#accept-game').hide();
		acceptPlayersNum += 1;
		$('.box'+acceptPlayersNum).addClass('declined');	
		socket.emit('declinedFFA');	
	});
	$('.create-lobby').on('click',function() {
		socket.emit('stopSearching');
		socket.emit('createLobby');
		createLobby();
		$('#searching-game').hide();
		$("#t").hide().timer("remove");
		$('#server-select').show();
		$('#mode-select').show();
		$('#search').show();
	});

    $(".basic").selectOrDie();
}

/*
 * Function: replaceBodyMenu
 * Redraw Body with menu
 */
function createLobby(){
	var jqXHR = $.get("/lobby.html?id=?"+username);
	jqXHR.done(function(text){
		$('#lobby').empty().prepend(text);
		$('#leave-lobby').on('click',function(){
			replaceBodyMenu(0);
		});
		data = "<div id='mode'><h3 id='mode-select'>Mode:</h3><select id='mode-select-select' class='basic''>"+
              "<option value='ffa'>Free for all</option>"+
              "<option value='dm' disabled>Deathmach</option>"+
             "<option value='unknown' disabled>Mode unaviable</option>"+
              "<option value='unknown' disabled>Mode unaviable</option>"+
            "</select>"+
          "</div>"+
          "<div class='cleaner'></div>"+
          "<div id='server'>"+
            "<h3 id='server-select'>Server:</h3>"+
           "<select id='server-select' class='basic'>"+
              "<option value='europe'>Europe</option>"+
             "<option value='america' disabled>America</option>"+
              "<option value='asia' disabled>Asia</option>"+
             "<option value='Australia' disabled>Australia</option>"+
            "</select>"+
            "<div class='lobby-settings-btn-send'>Save</div>";
		$('div #lobby-settings-btn').on('click',function(){
			console.log('klik')
			$('.notif').notifyMe(
				'right',
				'info',
				'Settins',
				data,
				200
			);
			$('.basic').selectOrDie();
			$('.lobby-settings-btn-send').on('click',function(){
				$('.notify').remove();  
			});
		});
	});
	
}

function replaceBodyMenu(time){
	context ="loading";
	setTimeout(function(){
		var jqXHR = $.get( "/game.html?id="+username);
		jqXHR.done(function(data){
			var html = data.search('<body');
			var script = data.search('<script');
			var replace = data.substring(html,script);
			$('body div').remove();
			$('body').prepend(replace);
			bindMenuEvents();
			$('#username').text(username);
			context = 'menu';
		});
	},time);
}


function clearAcceptMenu(){
	acceptPlayersNum = 0;
	$('#accepted-players li').removeClass('accepted');
	$('#accepted-players li').removeClass('declined');
}

function switchContext(item){
	$('#ranked').hide();
	$('#lobby').hide();
	$('#watch').hide();
	$('#shop').hide();
	$('#options').hide();
	$('#'+item).show()	;
}

function login(){
	$('#login').hide();
	$('.loading').show();
	socket.emit('login',{'username': $('#login #username').val(),'password':$('#password').val()});
	username = $('#login #username').val();
	return false;
}
function register(){
	if(validateEmail($('#mail').val())  == false){
		produceLoginError('registration','Wrong e-mail address');
		return 0;
	}
	if($('#registration #password').val() == '' ||  $('#registration #passwordagain').val() == '' || $('#registration #username').val() == ''){
		produceLoginError('registration','Need to be filled');
		return 0;
	}
	else{
		if($('#registration #password').val() !== $('#registration #passwordagain').val() ){
			produceLoginError('registration','Password doesn\'t match');
		}else{
			username = $('#registration #username').val();
			socket.emit('registerme',{'username': $('#registration #username').val(),'password':$('#registration #password').val(),'mail':$('#registration #mail').val()});	
		}
	}
}